<?php

/* Includes */
require_once './parsecsv-for-php/parsecsv.lib.php';
require_once './src/Production.php';
require_once './src/Staging.php';
require_once './src/Vagrant.php';
require_once './src/Importer.php';
require_once './src/FileSystem.php';
require_once './src/CategoryResolver.php';

class WebCallHandler {

    private $fileLocation;
    private $tmpFolder = "./tmp/";
    private $imageZip;
    private $images = "./images";
    private $tmpFilename = $tmpFilename = rand(0,20000).".json";

    public function __construct($env) {

        $this->env = new {$env};
        $importer = new Importer($this->env);

        $fs = new FileSystem;
        $cr = new CategoryResolver($this->env);
    }

    public function run()
    {
        if (!isset($this->imagesZip)) {
            exit();
        }

        if (!isset($this->fileLocation)) {
            exit();
        }

        $csv = new parseCSV($this->fileLocation);

        /* ----- Import data and convert JSON fields to array----- */
        foreach ($csv->data as $count => $c) {
            foreach ($c as $field => $line) {

                /* JSON detected */
                if ($importer->isJson($line)) {
                    $csv->data[$count][$field] = json_decode(json_encode(json_decode($line)));
                }
            }
        }

        /* ----- Add image IDs & Category ID----- */
        foreach ($csv->data as $row => $c) {

            /* Replace CSV data locations with IDs returned from APIs after upload */
            $csv->data[$row]['icon'] = $env->uploadImage($c['icon']);
            $csv->data[$row]['images'] = array($env->uploadImage($c['images']));
            $csv->data[$row]['categoryId'] = $cr->getCategoryId($c['category'], $c['vendorID'], $csv->data[$row]['icon']);

            $csv->data[$row]['links'][] = array(
                'url' => $c['URL'],
                'title' => $c['URLTitle']
            );

            unset($csv->data[$row]['URLTitle']);
            unset($csv->data[$row]['URL']);
        }

        $fh = fopen($tmpFolder.$tmpFilename, "w");
        fwrite($fh, json_encode($csv->data));

        /* Import into DB */
        exec("mongoimport -h ".$env->connectionString." -d ".$env->database." -c object.product -u ".$env->dbUser." -p ".$env->dbPass." --file ".$tmpFolder.$tmpFilename." --jsonArray");

        /* Clean up */
        unlink($tmpFolder.$tmpFilename);
        $fs->deleteDirectory($images);
    }

    public function prepareImages()
    {
        $zip = new ZipArchive;
        $zip->open($this->imagesZip);

        if ($zip != true) {
            echo "Zip file failure";
            exit();
        }

        $zip->extractTo($this->images);
        $zip->close();

    }

    public function setFileToProcess($fileLocation)
    {
        $this->$fileLocation = $fileLocation;
    }

    public function getFileToProcess()
    {
        return $this->fileLocation;
    }

    public function setEncoding($encoding = "UTF-8")
    {
        $this->encoding = $encoding;
    }

    public function getEncoding()
    {
        return $this->encoding;
    }

    public function setImagesZip($fileLocation)
    {
        $this->imageZip = $fileLocation;
    }

    public function getImagesZip()
    {
        return $this->imageZip;
    }

} 
<?php

class WebService
{
    public $token;
    public $apiUser = "ste@oneresult.co.uk";
    public $apiPass = "qwerty123";

    public function APIOauth()
    {
        $fields_string = "";
        $fields = array(
            'client_id' => urlencode("542041a096a55868770041a7_2olnwf3hw9kw8gwsg0k48cwkogw8wc4kk88ks8kcgos0o4w4s0"),
            'client_secret' => urlencode("3fms70hnsmuc8o4wsw448o0wcg4w4o8oookwkwcwwwkco888ss"),
            'username' => urlencode($this->apiUser),
            'password' => urlencode($this->apiPass),
            'grant_type' => urlencode("password")
        );

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiURL . "/avnetibm/user/token");
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $return = json_decode(curl_exec($ch));
        curl_close($ch);

        $this->token = $return->access_token;
    }

    public function createCategory($category, $parentCat = NULL, $vendorID, $iconID)
    {
        $fields_string = "";
        $url = $this->apiURL . "/avnetibm/objects/Category?flat=true&access_token=" . $this->token;
        $fields = array(
            'title' => urlencode($category),
            'vendorID' => urlencode($vendorID),
            'icon' => urlencode($iconID),
        );

        if (isset($parentCat)) {
            $fields['parentID'] = $parentCat->id;
        }

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $return = json_decode($result);

        return $return;
    }

    public function updateCategorySubcategories($parentCategory, $subCategory)
    {
        $i = 0;
        $fields_string = "";
        $url = $this->apiURL . "/avnetibm/objects/Category/" . $parentCategory->id . "?flat=true&access_token=" . $this->token;

        $fields = array(
            'title' => urlencode($parentCategory->title),
        );

        /* Re-add existing subcategories */
        if (isset($parentCategory->subCategories)) {
            foreach ($parentCategory->subCategories as $c) {
                $fields["subCategories[".$i."]"] = $c;
                ++$i;
            }

        }

        /* Add new sub-category */
        $fields["subCategories[".$i."]"] = $subCategory;

        $fields = array_unique($fields);

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $return = json_decode($result);
    }

    public function getCategoryData()
    {

        $request = curl_init($this->apiURL . "/avnetibm/objects/Category?flat=true&access_token=" . $this->token."&limit=0");
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($request);
        curl_close($request);

        $data = json_decode($return);

        foreach ($data as $v) {
            $return_data[$v->title] = $v;
        }

        if (isset($return_data)) {
            return $return_data;
        }
    }

    public function uploadImage($location)
    {
        echo "Location: ".$location."\n";

        $request = curl_init($this->apiURL."/avnetibm/files");
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt(
            $request,
            CURLOPT_POSTFIELDS,
            array(
                'file' => '@./images'.$location
            ));

        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($request);
        $imageInfo = json_decode($return);
        curl_close($request);

        return $imageInfo->id;
    }
}
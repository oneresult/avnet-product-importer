<?php

require_once 'WebService.php';

class Staging extends WebService {
    public $name = 'Staging';
    public $database = "ibmavnet_phase2";
    public $dbUser = "csvimport";
    public $dbPass = "qwerty";
    public $apiURL = "https://avnetappws-staging.channelmobile.co";
    public $connectionString = "ds039301.mongolab.com:39301";

    public function __construct()
    {
        $this->APIOauth();
    }
} 
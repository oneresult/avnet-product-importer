<?php


class Importer {

    public function __construct($env)
    {
        $this->env = $env;
    }

    public function getCategoryID($category)
    {

        $categoryFile = file('category_staging.json');

        if ($this->env->name == "production") {
            $categoryFile = file('category_production.json');
        }

        foreach ($categoryFile as $c) {
            $d[] = json_decode($c);
        };

        foreach ($d as $row) {
            foreach ($row->_id as $cID) {
                $categoryID = $cID;
            }

            if (strtolower($row->title) == strtolower($category)) {
                return $categoryID;
            }

        }
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

} 
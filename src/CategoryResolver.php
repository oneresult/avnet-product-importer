<?php

class CategoryResolver
{

    private $categories;

    public function __construct($env)
    {
        $this->env = $env;
        $this->categories = $this->env->getCategoryData();
    }

    private function splitCategoriesString($string)
    {
        $categories = explode("/", substr($string, 1, strlen($string)));
        array_pop($categories);

        return $categories;
    }

    public function getCategoryId($categoryString, $vendorID, $iconID)
    {
        $categories = $this->splitCategoriesString($categoryString);

        /* No subcategories */
        if (count($categories) == 1) {
            $catID = $this->singleCategory($categoryString, $vendorID, $iconID);
        } else {
            $catID = $this->multipleCategories($categories, $vendorID, $iconID);
        }

        $this->categories = $this->env->getCategoryData();

        return $catID;
    }

    private function singleCategory($category, $vendorID, $iconID)
    {
        $category = str_replace("/", "", $category);

        if ($catID = $this->doesCategoryExist($category)) {
            return $catID;
        } else {
            $catID = $this->env->createCategory($category, NULL, $vendorID, $iconID);
        }

        return $catID;
    }

    private function multipleCategories($categories, $vendorID, $iconID)
    {
        foreach ($categories as $k => $c) {

            /* Save previous category to use later in this loop (addCategoryToParent()) */
            if (isset($createdCat)) {
                $parentCat = $createdCat;
            }
            else {
                $parentCat = NULL;
            }

            /* Category exists */
            if ($a = $this->doesCategoryExist($c)) {
                $createdCat = $this->categories[$c];
            }
            /* Category doesn't exist */
            else {
                $createdCat = $this->env->createCategory($c, $parentCat, $vendorID, $iconID);
            }

            /* k == 0 if this is the root category, therefor it wont have a parent */
            if ($k != 0) {
                $this->addCategoryToParent($createdCat, $parentCat);
            }
        }

        return $createdCat->id;
    }

    private function doesCategoryExist($category)
    {
        foreach ($this->categories as $c) {

            $c->parentID = NULL;
            $c->vendorID = NULL;

            /* Category exists. Return it's ID */
            if ($c->title == $category) {
                return array(
                    $c->id,
                    $c->parentID,
                    $c->vendorID
                );
            }
        }
        return;
    }

    private function getCategoryByName($categoryName)
    {
        if ($this->categories[$categoryName]) {
            return $this->categories[$categoryName];
        }

        return;
    }

    private function addCategoryToParent($category, $parentCategory)
    {
        $this->env->updateCategorySubcategories($parentCategory, $category->id);

    }

}
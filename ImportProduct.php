<?php

//error_reporting(0);

/* Includes */
require_once './parsecsv-for-php/parsecsv.lib.php';
require_once './src/Production.php';
require_once './src/Staging.php';
require_once './src/Vagrant.php';
require_once './src/Importer.php';
require_once './src/FileSystem.php';
require_once './src/CategoryResolver.php';

/* Instantiation */
$env = new Staging;
$importer = new Importer($env);
$csv = new parseCSV('./import_data/14-05-2015-staging.csv');
$zip = new ZipArchive;
$fs = new FileSystem;
$cr = new CategoryResolver($env);

/* Options */
$csv->encoding('UTF-16', 'UTF-8');
$tmpFolder = "./.tmp/";
$tmpFilename = rand(0,20000).".json";
$imagesZip = "./import_images/2015-05-14_juniper_staging.zip";
$images = "./images/";

/* ----- Prepare images ----- */
$zip->open($imagesZip);
if ($zip != true) {
    echo "Zip file failure";
    exit();
}

$zip->extractTo($images);
$zip->close();

echo str_repeat("-=", 50)."\n\n";
echo "YOU ARE ABOUT TO UPLOAD TO '".$env->name."'\n\nDo you want to contnue? - ";
$handle = fopen ("php://stdin","r");
$line = fgets($handle);
if(strtolower(trim($line)) != 'yes'){
    echo "ABORTING!\n";
    exit;
}

/* ----- Import data and convert JSON fields to array----- */
foreach ($csv->data as $count => $c) {
    foreach ($c as $field => $line) {

        /* JSON detected */
        if ($importer->isJson($line)) {
            $csv->data[$count][$field] = json_decode(json_encode(json_decode($line)));
        }
    }
}

/* ----- Add image IDs & Category ID----- */
foreach ($csv->data as $row => $c) {

    /* Replace CSV data locations with IDs returned from APIs after upload */
        $csv->data[$row]['icon'] = $env->uploadImage($c['icon']);
    $csv->data[$row]['images'] = array($env->uploadImage($c['images']));
    $csv->data[$row]['categoryId'] = $cr->getCategoryId($c['category'], $c['vendorID'], $csv->data[$row]['icon']);

    $csv->data[$row]['links'][] = array(
        'url' => $c['URL'],
        'title' => $c['URLTitle']
    );

    unset($csv->data[$row]['URLTitle']);
    unset($csv->data[$row]['URL']);
    //echo "Done\n";
   echo "Done: ".$c['title'].PHP_EOL;
}


/* Write new CSV to file */
$fh = fopen($tmpFolder.$tmpFilename, "w");
fwrite($fh, json_encode($csv->data));

/* Import into DB */
exec("mongoimport -h ".$env->connectionString." -d ".$env->database." -c object.product -u ".$env->dbUser." -p ".$env->dbPass." --file ".$tmpFolder.$tmpFilename." --jsonArray");

/* Clean up */
unlink($tmpFolder.$tmpFilename);
$fs->deleteDirectory($images);



<?php

require_once './parsecsv-for-php/parsecsv.lib.php';

$csv = new parseCSV('../object.solution.csv');
$csv->encoding('UTF-16', 'UTF-8');
$tmpFilename = rand(0,20000).".json";
$connectionString = "ds039231.mongolab.com:39231";
$database = "csv_import_test";
$dbUser = "csvimport";
$dbPass = "qwerty";

foreach ($csv->data as $count => $c) {

	foreach ($c as $field => $line) {

		/* JSON detected */
		if (json_last_error() == JSON_ERROR_NONE && $line != "") {
			$csv->data[$count][$field] = json_decode(json_encode(json_decode($line)));
		}
	}
}

$fh = fopen(".".$tmpFilename, "w");
fwrite($fh, json_encode($csv->data));;

exec("mongoimport -h ".$connectionString." -d ".$database." -c object.solution -u ".$dbUser." -p ".$dbPass." --file .".$tmpFilename." --jsonArray");

unlink(".".$tmpFilename);
